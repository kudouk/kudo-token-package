package token

var auth0Domain string

type SetupParams struct {
	Auth0Domain string
}

func Setup(params SetupParams) {
	auth0Domain = params.Auth0Domain
}
