package token

import (
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"google.golang.org/grpc/metadata"
)

type TokenType int

const (
	TokenType_User TokenType = iota + 1
	TokenType_PasswordReset
	TokenType_Verify
	TokenType_Guest
	TokenType_System
)

func (tt TokenType) String() string {
	return [...]string{"", "user", "password-reset", "verify", "guest", "system"}[tt]
}

var TokenTypeNames = map[string]TokenType{
	"user":           TokenType_User,
	"password-reset": TokenType_PasswordReset,
	"verify":         TokenType_Verify,
	"guest":          TokenType_Guest,
	"system":         TokenType_System,
}

type Gateway int

const (
	Gateway_Admin Gateway = iota + 1
	Gateway_Customer
	Gateway_Supplier
	Gateway_ArtworkingUser
)

var GatewayTypeNames = map[string]Gateway{
	"admin":           Gateway_Admin,
	"customer":        Gateway_Customer,
	"supplier":        Gateway_Supplier,
	"artworking_user": Gateway_ArtworkingUser,
}

func (g Gateway) String() string {
	return [...]string{"", "admin", "customer", "supplier", "artworking_user"}[g]
}

type AccessTokenClaims struct {
	Gateway    Gateway   `json:"Gateway"`
	TokenType  TokenType `json:"TokenType"`
	Roles      []Role    `json:"Roles"`
	PersonalID int64     `json:"PersonalID"`
	OrgID      int64     `json:"OrgID"`
	Additional string    `json:"Additional"`
	jwt.StandardClaims
	keyPrefix string
}

func (t *AccessTokenClaims) SetKeyPrefix(keyPrefix string) {
	t.keyPrefix = keyPrefix
}

func (t AccessTokenClaims) GetGateway() Gateway {
	return t.Gateway
}

func (t AccessTokenClaims) GetJoinedRoles() string {
	strRoles := []string{}
	for _, r := range t.Roles {
		strRoles = append(strRoles, r.ToString())
	}
	return strings.Join(strRoles, ",")
}

func (t AccessTokenClaims) CheckGateway(testGateways ...Gateway) bool {
	if t.TokenType == TokenType_System {
		return true
	}

	passed := false
	for _, testGateway := range testGateways {
		if t.Gateway == testGateway {
			passed = true
		}
	}

	return passed
}

func (t AccessTokenClaims) CheckTokenType(testTokenTypes ...TokenType) bool {
	if t.TokenType == TokenType_System {
		return true
	}

	passed := false
	for _, testTokenType := range testTokenTypes {
		if t.TokenType == testTokenType {
			passed = true
		}
	}

	return passed
}

// Customer Token
type NewTokenClaimsParams struct {
	ExpirationLength time.Duration
	Gateway          Gateway
	TokenType        TokenType
	Roles            []Role
	PersonalID       int64
	OrgID            int64
	Additional       string
}

func NewTokenClaims(params NewTokenClaimsParams) *AccessTokenClaims {
	expirationTime := time.Now().Add(params.ExpirationLength)
	return &AccessTokenClaims{
		Gateway:    params.Gateway,
		TokenType:  params.TokenType,
		Roles:      params.Roles,
		PersonalID: params.PersonalID,
		OrgID:      params.OrgID,
		Additional: params.Additional,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
}

func (t *AccessTokenClaims) GetClaims() jwt.Claims {
	return t
}
func (t *AccessTokenClaims) UpdateClaims(c jwt.MapClaims) {
	if key, ok := c[t.keyPrefix+"PersonalID"]; ok {
		t.PersonalID = int64(key.(float64))
	}
	if key, ok := c[t.keyPrefix+"OrgID"]; ok {
		t.OrgID = int64(key.(float64))
	}
	if key, ok := c[t.keyPrefix+"TokenType"]; ok {
		t.TokenType = TokenType(int64(key.(float64)))
	}
	if key, ok := c[t.keyPrefix+"Gateway"]; ok {
		t.Gateway = Gateway(int64(key.(float64)))
	}
	if key, ok := c[t.keyPrefix+"Additional"]; ok {
		t.Additional = key.(string)
	}

	if key, ok := c[t.keyPrefix+"Roles"]; ok {
		roles := key.([]interface{})
		for _, r := range roles {
			if role, ok := r.(string); ok && role != "" {
				t.Roles = append(t.Roles, RoleNames[role])
			}
		}
	}
}
func (t *AccessTokenClaims) AddClaimsToHeader(r *http.Request) {
	r.Header.Add("Gateway", t.Gateway.String())
	r.Header.Add("TokenType", t.TokenType.String())
	r.Header.Add("Roles", t.GetJoinedRoles())
	r.Header.Add("PersonalID", strconv.Itoa(int(t.PersonalID)))
	r.Header.Add("OrgID", strconv.Itoa(int(t.OrgID)))
	r.Header.Add("Additional", t.Additional)
}
func (t *AccessTokenClaims) GetMetadataPairs() metadata.MD {
	return metadata.Pairs(
		"Gateway", t.Gateway.String(),
		"TokenType", t.TokenType.String(),
		"Roles", t.GetJoinedRoles(),
		"PersonalID", strconv.Itoa(int(t.PersonalID)),
		"OrgID", strconv.Itoa(int(t.OrgID)),
		"Additional", t.Additional,
	)
}

func BuildMetadataPairs(r *http.Request) metadata.MD {
	Gateway := r.Header.Get("Gateway")
	TokenType := r.Header.Get("TokenType")
	Roles := r.Header.Get("Roles")
	PersonalID := r.Header.Get("PersonalID")
	OrgID := r.Header.Get("OrgID")
	Additional := r.Header.Get("Additional")

	return metadata.Pairs(
		"Gateway", Gateway,
		"TokenType", TokenType,
		"Roles", Roles,
		"PersonalID", PersonalID,
		"OrgID", OrgID,
		"Additional", Additional,
	)
}
