package token

import (
	"context"

	"github.com/mikespook/gorbac"
)

func HasCorrectRoles(ctx context.Context, rbac *gorbac.RBAC, permission gorbac.Permission) (*AccessTokenClaims, bool) {
	tokenClaims, _ := GetAccessTokenFromContext(ctx)
	if tokenClaims == nil {
		return nil, false
	}

	if isSystem := tokenClaims.CheckTokenType(TokenType_System); isSystem {
		return tokenClaims, true
	}

	rolesStr := []string{}
	for _, role := range tokenClaims.Roles {
		rolesStr = append(rolesStr, role.ToString())
	}

	if ok := gorbac.AnyGranted(rbac, rolesStr, permission, nil); !ok {
		return nil, false
	}

	return tokenClaims, true
}
