package token

import (
	"context"
	"net/http"
	"regexp"

	"google.golang.org/grpc/metadata"
)

func AuthInterceptor(
	next http.Handler,
	jwtKey string,
) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		path := r.URL.Path

		tokenClaims := &AccessTokenClaims{}

		match, _ := regexp.MatchString("/api/p//*", path)
		if match {
			err := CheckAuthToken(r, tokenClaims, jwtKey)
			if err == nil {
				tokenClaims.AddClaimsToHeader(r)
			}
			next.ServeHTTP(w, r)
			return
		}

		if err := CheckAuthToken(r, tokenClaims, jwtKey); err != nil {
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			return
		}
		tokenClaims.AddClaimsToHeader(r)

		next.ServeHTTP(w, r)
	})
}

func GatewayMetadataAnnotator() func(_ context.Context, r *http.Request) metadata.MD {
	return func(_ context.Context, r *http.Request) metadata.MD {
		md := BuildMetadataPairs(r)
		return md
	}
}
