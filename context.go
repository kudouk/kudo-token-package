package token

import (
	"context"
	"fmt"
	"strings"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type contextKey string

func (c contextKey) String() string {
	return "kudotoken-" + string(c)
}

var (
	contextKeyTokenClaims = contextKey("token-claims")
)

func AccessTokenClaimsToContext(jwtKey string) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		tokenClaims := newAccessTokenClaimsFromContext(ctx, jwtKey)
		return handler(context.WithValue(ctx, contextKeyTokenClaims, tokenClaims), req)
	}
}

func GetAccessTokenFromContext(ctx context.Context) (*AccessTokenClaims, bool) {
	tokenClaims, ok := ctx.Value(contextKeyTokenClaims).(*AccessTokenClaims)
	return tokenClaims, ok
}

func newAccessTokenClaimsFromContext(ctx context.Context, jwtKey string) *AccessTokenClaims {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil
	}

	tokenClaims := AccessTokenClaims{}

	token, authorigin, ok := getAuthTokenFromMD(md)
	if !ok {
		return nil
	}

	if authorigin == "kudo" {
		// Now parse the token
		if err := ParseToken(token, &tokenClaims, jwtKey); err != nil {
			fmt.Println("error parsing token", err)
			return nil
		}

	} else if authorigin == "auth0" {
		// Now parse the token
		tokenClaims.SetKeyPrefix("https://artworker.io/")
		if err := ParseAuth0Token(token, &tokenClaims); err != nil {
			fmt.Println("error parsing token", err)
			return nil
		}
	}

	return &tokenClaims

	// if noAuth {
	// 	if err := CheckAuthToken(r, tokenClaims, jwtKey); err != nil {
	// 		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
	// 		return
	// 	}
	// 	tokenClaims.AddClaimsToHeader(r)
	// }

	// valueStrings := md.Get("TokenType")
	// if len(valueStrings) != 0 {
	// 	tokenClaims.TokenType = TokenTypeNames[valueStrings[0]]
	// }

	// valueStrings = md.Get("Gateway")
	// if len(valueStrings) != 0 {
	// 	tokenClaims.Gateway = GatewayTypeNames[valueStrings[0]]
	// }

	// if tokenClaims.Gateway == 0 && tokenClaims.TokenType == 0 {
	// 	return nil
	// }

	// valueStrings = md.Get("PersonalID")
	// if len(valueStrings) != 0 {
	// 	if n, err := strconv.ParseInt(valueStrings[0], 10, 64); err == nil {
	// 		tokenClaims.PersonalID = n
	// 	}
	// }

	// valueStrings = md.Get("OrgID")
	// if len(valueStrings) != 0 {
	// 	if n, err := strconv.ParseInt(valueStrings[0], 10, 64); err == nil {
	// 		tokenClaims.OrgID = n
	// 	}
	// }

	// valueStrings = md.Get("Roles")
	// if len(valueStrings) != 0 {
	// 	roles := strings.Split(valueStrings[0], ",")
	// 	for _, role := range roles {
	// 		tokenClaims.Roles = append(tokenClaims.Roles, RoleNames[role])
	// 	}
	// }

	// return &tokenClaims
}

// func CheckTokenType(ctx context.Context, testTypes ...TokenType) bool {
// 	tokenType, ok := GetTokenTypeFromContext(ctx)
// 	if !ok {
// 		return false
// 	}
// 	if tokenType == TokenType_System {
// 		return true
// 	}

// 	passed := false
// 	for _, testType := range testTypes {
// 		if tokenType == testType {
// 			passed = true
// 		}
// 	}

// 	return passed
// }

// func GetTokenTypeFromContext(ctx context.Context) (TokenType, bool) {
// 	md, ok := metadata.FromIncomingContext(ctx)
// 	if !ok {
// 		return 0, false
// 	}

// 	typeStrings := md.Get("TokenType")
// 	if len(typeStrings) == 0 {
// 		return 0, false
// 	}

// 	tokenType := TokenTypeNames[typeStrings[0]]

// 	return tokenType, true
// }

func AppendToContextAsSystem(ctx context.Context, token string, kv ...string) context.Context {
	kv = append(kv, "TokenType", TokenType_System.String())
	kv = append(kv, "Authorization", "bearer "+token)
	return metadata.AppendToOutgoingContext(ctx, kv...)
}

func AppendToContext(ctx context.Context, kv ...string) context.Context {
	return metadata.AppendToOutgoingContext(ctx, kv...)
}

func getAuthTokenFromMD(md metadata.MD) (string, string, bool) {
	authorizationStrings := md.Get("authorization")
	authOriginStrings := md.Get("auth-origin")
	authOrigin := "kudo"
	if len(authOriginStrings) != 0 {
		authOrigin = authOriginStrings[0]
	}

	if len(authorizationStrings) == 0 {
		return "", authOrigin, false
	}

	auth := strings.Split(authorizationStrings[0], " ")

	if len(auth) != 2 {
		return "", authOrigin, false
	}

	return auth[1], authOrigin, true
}
