package token

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

var FOUR_YEARS = time.Hour * 60 * 24 * 365 * 4

type TokenClaims interface {
	GetGateway() Gateway
	GetClaims() jwt.Claims
	UpdateClaims(t jwt.MapClaims)
}

func GenerateToken(tokenClaims TokenClaims, jwtKey string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, tokenClaims.GetClaims())
	return token.SignedString([]byte(jwtKey))
}

func GenerateSystemToken(jwtKey string, params *NewTokenClaimsParams) string {
	if params == nil {
		params = &NewTokenClaimsParams{}
	}

	params.TokenType = TokenType_System
	params.ExpirationLength = time.Duration(FOUR_YEARS)
	params.Roles = []Role{}

	if jwtKey == "" {
		panic("no jwtKey was passed to token.GenerateSystemToken")
	}

	tokenClaims := NewTokenClaims(*params)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, tokenClaims.GetClaims())
	res, err := token.SignedString([]byte(jwtKey))
	if err != nil {
		panic("error signing system token")
	}
	return res
}

func GetAuthTokenFromRequest(r *http.Request) (string, string, error) {
	authToken := r.Header.Get("Authorization")
	authOrigin := r.Header.Get("auth-origin")
	if authOrigin == "" {
		authOrigin = "kudo"
	}

	if len(authToken) == 0 {
		return "", authOrigin, errors.New("authorization header not found")
	}

	authHeaderParts := strings.Split(authToken, " ")
	if len(authHeaderParts) != 2 || strings.ToLower(authHeaderParts[0]) != "bearer" {
		return "", authOrigin, errors.New("authorization header format must be Bearer {token}")
	}

	return authHeaderParts[1], authOrigin, nil
}

func ParseToken(tokenString string, tokenClaims TokenClaims, jwtKey string) error {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(jwtKey), nil
	})

	if token == nil || !token.Valid {
		if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors&jwt.ValidationErrorMalformed != 0 {
				return fmt.Errorf("token is not valid")
			} else if ve.Errors&jwt.ValidationErrorExpired != 0 {
				// Token is expired, lets refresh it.
				if claims, ok := token.Claims.(jwt.MapClaims); ok {
					tokenClaims.UpdateClaims(claims)
					return fmt.Errorf("token is expired")
				}
				return fmt.Errorf("token is expired")
			} else {
				return fmt.Errorf("couldn't handle this token")
			}
		}
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok {
		tokenClaims.UpdateClaims(claims)
		return nil
	}
	return fmt.Errorf("unexpected error getting claims from token")
}

func CheckAuthToken(
	r *http.Request,
	tokenClaims TokenClaims,
	jwtKey string,
) error {
	token, origin, err := GetAuthTokenFromRequest(r)
	if err != nil {
		return err
	}

	if origin == "kudo" {
		// Now parse the token
		if err := ParseToken(token, tokenClaims, jwtKey); err != nil {
			fmt.Println("error parsing token", err)
			return err
		}

	} else if origin == "auth0" {
		// Now parse the token
		if err := ParseAuth0Token(token, tokenClaims); err != nil {
			fmt.Println("error parsing token", err)
			return err
		}
	}

	return nil
}
