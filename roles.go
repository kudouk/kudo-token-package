package token

type Role string

const (
	Role_TenderSupplier             Role = "tender-supplier"
	Role_TenderAdmin                Role = "tender-admin"
	Role_Task                       Role = "spec-task"
	Role_SpecificationAdmin         Role = "spec-admin"
	Role_SubmitArtwork              Role = "artwork-submit"
	Role_SubmitFinalArtworkApprover Role = "artwork-final-approver"
	Role_ProvisionalArtworkApprover Role = "artwork-prov-approver"
	Role_OrderCustomer              Role = "order-customer"
	Role_OrderItemSupplier          Role = "order-item-supplier"
	Role_OrderItemAdmin             Role = "order-item-admin"
	Role_CustomerCart               Role = "cart-customer"
	Role_CustomerPayment            Role = "payment-customer"
	Role_AdminProduct               Role = "admin-product"
	Role_AdminAttribute             Role = "admin-attr"
	Role_AttributeViewer            Role = "viewer-attr"
	Role_AdminFileRequirement       Role = "admin-file-req"
	Role_ViewerFileRequirement      Role = "viewer-file-req"
	Role_CouponAdmin                Role = "coupon-admin"
	Role_BespokeJobCustomer         Role = "bespoke-job-customer"
	Role_Customer                   Role = "customer"
	Role_ViewCustomers              Role = "view-customers"
	Role_AdminCapability            Role = "admin-capabilities"
	Role_Media                      Role = "media"
	Role_SupplierCompany            Role = "supplier-company"
	Role_SupplierCompanyAdmin       Role = "supplier-company-admin"
	Role_SupplierTempCompany        Role = "supplier-temp-company"
	Role_IssueRefund                Role = "issue-refund"
	Role_CancelOrderItem            Role = "order-item-cancel"
	// artworking
	Role_ArtworkingUser       Role = "artworking-user"
	Role_AdminArtworkingUsers Role = "admin-artworking-user"
	Role_ArtworkRequest       Role = "artwork-request-user"
)

func (r Role) ToString() string {
	return string(r)
}

var RoleNames = map[string]Role{
	"tender-supplier":        Role_TenderSupplier,
	"tender-admin":           Role_TenderAdmin,
	"spec-task":              Role_Task,
	"spec-admin":             Role_SpecificationAdmin,
	"artwork-submit":         Role_SubmitArtwork,
	"artwork-final-approver": Role_SubmitFinalArtworkApprover,
	"artwork-prov-approver":  Role_ProvisionalArtworkApprover,
	"order-customer":         Role_OrderCustomer,
	"order-item-supplier":    Role_OrderItemSupplier,
	"order-item-admin":       Role_OrderItemAdmin,
	"cart-customer":          Role_CustomerCart,
	"payment-customer":       Role_CustomerPayment,
	"admin-product":          Role_AdminProduct,
	"admin-attr":             Role_AdminAttribute,
	"admin-file-req":         Role_AdminFileRequirement,
	"viewer-attr":            Role_AttributeViewer,
	"viewer-file-req":        Role_ViewerFileRequirement,
	"coupon-admin":           Role_CouponAdmin,
	"bespoke-job-customer":   Role_BespokeJobCustomer,
	"customer":               Role_Customer,
	"view-customers":         Role_ViewCustomers,
	"admin-capabilities":     Role_AdminCapability,
	"media":                  Role_Media,
	"supplier-company":       Role_SupplierCompany,
	"supplier-company-admin": Role_SupplierCompanyAdmin,
	"supplier-temp-company":  Role_SupplierTempCompany,
	"issue-refund":           Role_IssueRefund,
	"order-item-cancel":      Role_CancelOrderItem,
	// artworking
	"artworking-user":       Role_ArtworkingUser,
	"admin-artworking-user": Role_AdminArtworkingUsers,
}
