package token

import (
	"context"
)

func (t *AccessTokenClaims) BuildTestContext(ctx *context.Context) context.Context {
	if ctx == nil {
		bg := context.Background()
		ctx = &bg
	}
	return context.WithValue(*ctx, contextKeyTokenClaims, t)
}
