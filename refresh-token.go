package token

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

type RefreshTokenClaims struct {
	RefreshToken string
	jwt.StandardClaims
}

func (t *RefreshTokenClaims) GetGateway() Gateway {
	return Gateway_Admin
}

func (t *RefreshTokenClaims) GetClaims() jwt.Claims {
	return t
}

func (t *RefreshTokenClaims) UpdateClaims(c jwt.MapClaims) {
	t.RefreshToken = c["RefreshToken"].(string)
	t.ExpiresAt = int64(c["exp"].(float64))
}

// Customer Token
type NewRefreshTokenClaimsParams struct {
	ExpirationLength time.Duration
	RefreshToken     string
}

func NewRefreshTokenClaims(params NewRefreshTokenClaimsParams) *RefreshTokenClaims {
	expirationTime := time.Now().Add(params.ExpirationLength)
	return &RefreshTokenClaims{
		RefreshToken: params.RefreshToken,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
}

func GenerateRefreshToken(tokenClaims *RefreshTokenClaims, jwtKey string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, tokenClaims.GetClaims())
	return token.SignedString([]byte(jwtKey))
}

// func ParseToken(tokenString string, tokenClaims *RefreshTokenClaims, jwtKey string) error {
// 	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
// 		// Don't forget to validate the alg is what you expect:
// 		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
// 			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
// 		}

// 		return []byte(jwtKey), nil
// 	})

// 	if !token.Valid {
// 		if ve, ok := err.(*jwt.ValidationError); ok {
// 			if ve.Errors&jwt.ValidationErrorMalformed != 0 {
// 				return fmt.Errorf("Token is not valid")
// 			} else if ve.Errors&jwt.ValidationErrorExpired != 0 {
// 				// Token is expired, lets refresh it.
// 				if claims, ok := token.Claims.(jwt.MapClaims); ok {
// 					tokenClaims.UpdateClaims(claims)
// 					return fmt.Errorf("Token is expired")
// 				}
// 				return fmt.Errorf("Token is expired")
// 			} else {
// 				return fmt.Errorf("Couldn't handle this token")
// 			}
// 		}
// 	}

// 	if claims, ok := token.Claims.(jwt.MapClaims); ok {
// 		tokenClaims.UpdateClaims(claims)
// 		return nil
// 	}
// 	return fmt.Errorf("Unexpected error getting claims from token")
// }
